# Docker - Environment Variables

This activity assumes that you are running inside bash or zsh inside
Docker-in-Docker DevContainer. So please start one now and open a terminal
inside.

Containers are often designed to be configured through environment
variables. So let's learn about environment variables and how to
pass them into our containers.

1. Look at the documentation for the containers listed below. For each
    list two environment variables that they use.

    * `nginx`
        * ...
        * ...
    * `mongo`
        * ...
        * ...

## Listing environment variables

2. In Linux we can get a list of our environment variables and their values
    using `printenv`. Run the following in your terminal.

    ```
    printenv
    ```

    * About how many environment variables are defined?
    * List a couple environment variables here.

## Container environment variables

3. Use `docker run` to start a busybox container and list the environment
    variables avaiable inside. (Remember to use `-it` to attach your
    terminal to an interactive session inside busybox).

    * About how many environment variables are defined?
    * Are any of them defined in your host environment that you listed
        in the previous question? How many?
    * Of those that are defined in your host environment, do they have
        the same value as within busybox?

Before continuing, exit and remove the busybox container.

## Define an environment variable

In Linux we define an environment variables as follows.

```
VAR=VAL
```

For example, I can create an environment variable with my cat's name as
follows.

```
CAT=Kitara
```

4. Create a varilable with a pet's name.

NOTE: Your variable will not appear in `printenv`... yet.

## Displaying a variable value

To access the value of a variable, we prefix it with `$`. However,
if you did the following

```
$CAT
```

Your shell would replace `$CAT` with its value `Kitara` and then try to
run that as a command, since it is the first value on the line.

So if we want to view the value in our variable, we'll need to print
it out with echo.

```
echo $CAT
```

5. Print the value of your pet variable.

## Exporting a variable

Right now your environment variable is not available to commands you
run. Let's prove this to ourselves.

6. Create a file named `pet.bash` with the following contents (replace
    CAT with your pet variable).

    ```
    echo CAT is $CAT
    echo PATH is $PATH
    ```

    Now run `pet.bash` as follows.

    ```
    bash pet.bash
    ```

    You should see something like.

    ```
    CAT is
    PATH is /usr/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin ...
    ```

    Notice the script has access to the PATH variable, but not your pet
    variable.

We must export a variable to make it available to commands we run.

```
export CAT
```

Let's convince ourselves that this works.

7. Export your pet variable, and then run the `pet.bash` script again.
    This time, you should see your pet's name.

Often when we create environment variables, we want to export them too.
We can do this in a single statement as follows.

```
export CAT=Kitara
```

8. An exported variable also appears in `printenv`'s output. Try it.

9. Now, are exported environment variables passed through to a Docker
    container? Start a busybox container and run `printenv` inside.

Before continuing, exit and remove your busybox container.

## Passing environment variables into a Docker container

We can pass an environment variable into a contain when we create it
using the `-e` or `--env` option.

```
docker create -e CAT busybox
```

Since `docker run` both creates and runs a container, we can use `-e` and
`--env` with it as well.

```
docker run -it --rm -e CAT busybox
```

10. Pass your pet variable into a busybox container, and print its environment
    confirm that it is available inside the container. Exit and stop your
    container when you are done.

We can pass multiple environment variables by using `-e` repeatedly. We
can also define new variables or pass a different value than what is in
our current environment at the same time. For example, let's pass our
pet variable with a different name, and let's pass a new variable DOG
with a value of Fido.

```
docker run -it --rm -e CAT=Claws -e DOG=Fido busybox
```

11. Try it the above command and confirm that it works. Exit and remove
    the container when you are done.

## Env files

A set of environment variables and their values that we are passing
to a container represents a configuration of the container we are
creating. If we have a lot of environment variables, our command will
become long and messy.

Docker allows us to place environment variables we want to pass to
a new container in an `env` file. This is a plain text file that defines
environment variables one per line. Here is an example of an `env` file.

```
CAT
DOG=Fido
```

Here we are asking that our CAT environment be passed through and we are
defining DOG to be Fido.

When we create the container, we use the `--env-file` option to identify
the file we want docker to use. Assuming the above file is nmaed `pets.env`,
we could create and run our container as follows.

```
docker run -it --rm --env-file=pets.env busybox
```

12. Create `pets.env` as shown above (but with your pet variables),
    create and run a busybox container, and confirm that these variables
    are now available.

    ```
    CAT
    DOG=Fido
    ```

## Summary

13. Summarize what you have learned.
