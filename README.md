# Docker

## Order of Activities

* Managing Images and Containers
* Bind Mounting
* Ports
* Environment
* Compose
* Building Images
* Dev Containers
